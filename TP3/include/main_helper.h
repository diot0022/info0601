#ifndef __MAIN_HELPER__
#define __MAIN_HELPER__

int open_file(char* file_name);
void close_file(int fd);
void empty_stdin();
int choice(int i);
char* get_str(int max_size);

#endif