#ifndef __FILE_FUNCTIONS__
#define __FILE_FUNCTIONS__

#include <unistd.h>

typedef struct {
    long size;
    off_t address;
} addr_t;

typedef struct {
    int table_size;
    addr_t* addresses;
    off_t next_table;
} table_t;

/*write_table
    Will write a new address table into the file at the current position.
    fd is a file descriptor
    offset is the offset at which the current position will be written (ignored if -1)
    table_size is the number of addresses to write
*/
void write_table(int fd, off_t offset, int table_size);

/*write_void_table
    Will write a new void table into the file at the current position.
    fd is a file descriptor
    offset is the offset at which the current position will be written (ignored if -1)
    table_size is the number of void addresses to write
*/
void write_void_table(int fd, off_t offset, int n);

/*find_available_entry
    Will search for a new address entry in all the address tables.
    fd is a file descriptor

    Returns an offset if successful
    Returns -1 if no offset has been found
*/
off_t find_available_entry(int fd);

/*find_available_void_entry
    Will search for a new void entry in all the void tables.
    fd is a file descriptor

    Returns an offset if successful
    Returns -1 if no offset has been found
*/
off_t find_available_void_entry(int fd);

/*add_str
    Will find for an available entry to add a string. If no address entry is available, a
    new table will be created, and the chosen entry will be the first one of this table.
    fd is a file descriptor
    message is the message to write in the file
*/
void add_str(int fd, char* message);

/*read_table
    Will read a table at the current position in the file descriptor, and store it in table.
    The addresses field has to be freed after using table, since it is dynamically allocated
    fd is a file descriptor
    table is the table in which the read informations will get stored

    Returns the offset to the next table's offset in the current table
*/
off_t read_table(int fd, table_t* table);

/*append_table
    Will append a new address table of size table_size to EOF. The new table's offset will be written
    to the previous one
    fd is a file descriptor
    table_size is the size of the table to be appended
*/
void append_table(int fd, int table_size);

/*append_void_table
    Will append a new void table of size table_size to EOF. The new table's offset will be written
    to the previous one
    fd is a file descriptor
    table_size is the size of the table to be appended
*/
void append_void_table(int fd, int table_size);

/*print_all_str
    Will print all str stored in file by reading all addresses in the tables.
    fd is a file descriptor
*/
void print_all_str(int fd);

/*print_stats
    Will print useful debugging informations, like the number of occupied addresses, the number of tables...
    fd is a file descriptor
*/
void print_stats(int fd);

#endif