#include <stdio.h>
#include <stdlib.h>
#include "file_functions.h"
#include "main_helper.h"

int main(int argc, char* argv[])
{
    int fd;
    int selection;
    char* str;

    if (argc != 2) {
        fprintf(stderr, "Usage:\t./ex2 <file_name>\n");
        exit(EXIT_FAILURE);
    }

    fd = open_file(argv[1]);

    selection = 0;
    while (selection != -1) {
        puts("\n1) File stats");
        puts("2) Add table");
        puts("3) Add void table");
        puts("4) Find available entry");
        puts("5) Find available void entry");
        puts("6) Add string");
        puts("7) Print all strings");

        switch (selection = choice(7)) {
            case 1:
                print_stats(fd);
                break;
            case 2:
                puts("Adding table with 8 addresses...");
                append_table(fd, 8);
                break;
            case 3:
                puts("Adding void table with 8 addresses...");
                append_void_table(fd, 8);
                break;
            case 4:
                printf("Available entry at: %ld\n", find_available_entry(fd));
                break;
            case 5:
                printf("Available entry at: %ld\n", find_available_void_entry(fd));
                break;
            case 6:
                str = get_str(256);
                add_str(fd, str);
                free(str);
                break;
            case 7:
                print_all_str(fd);
                break;
            case -1:
                puts("Bye bye!");
                break;
        }
    }

    close_file(fd);

    return EXIT_SUCCESS;
}