#include "file_functions.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

void write_table(int fd, off_t offset, int table_size)
{
    off_t cur_pos;
    char* arr;
    unsigned int x;
    size_t size;

    size = sizeof(int) + table_size * sizeof(addr_t) + sizeof(off_t); // (+1 for size and +1 for next table)
    arr = malloc(size); // Array to write to the file
    if (arr == NULL) {
        fprintf(stderr, "malloc failed: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    for (x = 0; x < size; x++) arr[x] = 0;

    *((int*)arr) = table_size; // Writing array size at beginning of file

    if (offset != -1) {
        cur_pos = lseek(fd, 0L, SEEK_CUR); // Getting current pos
        if (cur_pos == -1) {
            fprintf(stderr, "lseek syscall failed (storing current position): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }   

        if (lseek(fd, offset, SEEK_SET) == -1) {
            fprintf(stderr, "lseek syscall failed (offset position): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Going to offset

        if (write(fd, &cur_pos, sizeof(off_t)) == -1) {
            fprintf(stderr, "write syscall failed (writing current position): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Writing current pos to offset (to get table from offset)

        if (lseek(fd, cur_pos, SEEK_SET) == -1) {
            fprintf(stderr, "lseek syscall failed (to current position): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Going back to current pos
    }

    if (write(fd, arr, size) == -1) {
        fprintf(stderr, "write syscall failed (writing table): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Writing to file

    free(arr);
}

void write_void_table(int fd, off_t offset, int n)
{
    write_table(fd, offset, n);
}

off_t find_available_entry(int fd)
{
    int x;
    off_t cur_pos, next_table;
    table_t table;

    if ((cur_pos = lseek(fd, 0L, SEEK_CUR)) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Store current position

    if (lseek(fd, 0L, SEEK_SET)) {
        fprintf(stderr, "lseek syscall failed (beginning of file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to beginning of file

    do {
        x = 0;
        next_table = read_table(fd, &table); // Read current table & store offset to offset of next one

        if (table.next_table != 0L) {
            if (lseek(fd, table.next_table, SEEK_SET) == -1) {
                fprintf(stderr, "lseek syscall failed (to next table): %s\n", strerror(errno));
                exit(EXIT_FAILURE);
            } // Go to next address table
        }

        while (x < table.table_size && table.addresses[x].size != 0) x++; // Check for available entry

        free(table.addresses);
    } while (table.next_table != 0L && x == table.table_size); // If not found && next table then iterate again

    if (lseek(fd, cur_pos, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (current position) la: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go back to current position

    if (x == table.table_size) return -1; // No addresses found

    return next_table - (table.table_size - x) * sizeof(addr_t); // Returning offset to entry
}

off_t find_available_void_entry(int fd)
{
    int x;
    off_t cur_pos, next_table;
    table_t table;

    if ((cur_pos = lseek(fd, 0L, SEEK_CUR)) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Store current position

    if (lseek(fd, 0L, SEEK_SET)) {
        fprintf(stderr, "lseek syscall failed (beginning of file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to beginning of file

    if (read(fd, &x, sizeof(int)) == -1) {
        fprintf(stderr, "lseek syscall failed (beginning of file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Getting address table size to reposition cursor to the first void table

    if (lseek(fd, sizeof(addr_t) * x + sizeof(off_t), SEEK_CUR) == -1) {
        fprintf(stderr, "lseek syscall failed (to first void table): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to first void table

    do {
        x = 0;
        next_table = read_table(fd, &table); // Read current table & store offset to offset of next one

        if (table.next_table != 0L) {
            if (lseek(fd, table.next_table, SEEK_SET) == -1) {
                fprintf(stderr, "lseek syscall failed (to next table): %s\n", strerror(errno));
                exit(EXIT_FAILURE);
            } // Go to next address table
        }

        while (x < table.table_size && table.addresses[x].size != 0) x++; // Check for available entry

        free(table.addresses);
    } while (table.next_table != 0L && x == table.table_size); // If not found && next table then iterate again

    if (lseek(fd, cur_pos, SEEK_SET)) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go back to current position

    if (x == table.table_size) return -1; // No addresses found

    return next_table - (table.table_size - x) * sizeof(addr_t); // Returning offset to entry
}

off_t read_table(int fd, table_t* table)
{
    off_t cur_pos;

    if ((cur_pos = lseek(fd, 0, SEEK_CUR)) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Store current position

    if (read(fd, &table->table_size, sizeof(int)) == -1) {
        fprintf(stderr, "read syscall failed (read size): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Read table size

    if ((table->addresses = malloc(sizeof(addr_t) * table->table_size)) == NULL) {
        fprintf(stderr, "malloc syscall failed (allocating table): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Allocate whole table

    if (read(fd, table->addresses, sizeof(addr_t) * table->table_size) == -1) {
        fprintf(stderr, "read syscall failed (read addresses): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Read table addresses

    if (read(fd, &table->next_table, sizeof(off_t)) == -1) {
        fprintf(stderr, "read syscall failed (read next table offset): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Read next table offset

    if (lseek(fd, cur_pos, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go back to current position

    return cur_pos + sizeof(int) + sizeof(addr_t) * table->table_size; // Return offset to next table
}

void append_table(int fd, int table_size)
{
    off_t cur_pos;
    off_t write_to;
    table_t table;

    if ((cur_pos = lseek(fd, 0L, SEEK_CUR)) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Store current position

    if (lseek(fd, 0L, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (at start): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to first address table

    do {
        write_to = read_table(fd, &table); // Read table

        if (lseek(fd, table.next_table, SEEK_SET) == -1) {
            fprintf(stderr, "lseek syscall failed (to next table): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Go to next address table

        free(table.addresses);
    } while (table.next_table != 0L);

    if (lseek(fd, 0L, SEEK_END) == -1) {
        fprintf(stderr, "lseek syscall failed (to EOF): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to EOF

    write_table(fd, write_to, table_size); // Write new address table
}

void append_void_table(int fd, int table_size)
{
    off_t cur_pos;
    off_t write_to;
    int x;
    table_t table;

    if ((cur_pos = lseek(fd, 0L, SEEK_CUR)) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Store current position

    if (lseek(fd, 0L, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (at start): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to beginning of file

    if (read(fd, &x, sizeof(int)) == -1) {
        fprintf(stderr, "lseek syscall failed (beginning of file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Getting address table size to reposition cursor to the first void table

    if (lseek(fd, sizeof(addr_t) * x + sizeof(off_t), SEEK_CUR) == -1) {
        fprintf(stderr, "lseek syscall failed (to first void table): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to first void table

    do {
        write_to = read_table(fd, &table); // Read table

        if (lseek(fd, table.next_table, SEEK_SET) == -1) {
            fprintf(stderr, "lseek syscall failed (to next table): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Go to next address table

        free(table.addresses);
    } while (table.next_table != 0L);

    if (lseek(fd, 0L, SEEK_END) == -1) {
        fprintf(stderr, "lseek syscall failed (to EOF): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to EOF

    write_void_table(fd, write_to, table_size); // Write new address table
}

void print_stats(int fd)
{
    off_t cur_pos;
    int n_address_table, n_addresses, n_void_table, n_void_addresses, x;
    table_t table;

    if ((cur_pos = lseek(fd, 0L, SEEK_CUR)) == -1) {
        fprintf(stderr, "lseek syscall failed (current pos): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (lseek(fd, 0L, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (to beginning of file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to beginning of file

    n_address_table = 0;
    n_addresses = 0;

    do {
        read_table(fd, &table); // Read current table

        if (lseek(fd, table.next_table, SEEK_SET) == -1) {
            fprintf(stderr, "lseek syscall failed (to next table): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Go to next address table

        for (x = 0; x < table.table_size; x++) {
            if (table.addresses[x].size != 0) n_addresses++;
        }

        free(table.addresses);

        n_address_table++;
    } while (table.next_table != 0L);

    if (lseek(fd, 0L, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (to beginning of file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to beginning of file

    if (read(fd, &x, sizeof(int)) == -1) {
        fprintf(stderr, "lseek syscall failed (beginning of file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Getting address table size to reposition cursor to the first void table

    if (lseek(fd, sizeof(addr_t) * x + sizeof(off_t), SEEK_CUR) == -1) {
        fprintf(stderr, "lseek syscall failed (to first void table): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to first void table

    n_void_table = 0;
    n_void_addresses = 0;

    do {
        read_table(fd, &table); // Read current table

        if (lseek(fd, table.next_table, SEEK_SET) == -1) {
            fprintf(stderr, "lseek syscall failed (to next table): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Go to next address table

        for (x = 0; x < table.table_size; x++) {
            if (table.addresses[x].size != 0) n_void_addresses++;
        }

        free(table.addresses);

        n_void_table++;
    } while (table.next_table != 0L);

    if (lseek(fd, cur_pos, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go back to current position

    printf("Number of tables: %d\nAddresses occupied: %d\nNumber of void tables: %d\nVoid addresses occupied: %d\n", n_address_table, n_addresses, n_void_table, n_void_addresses);
}

void add_str(int fd, char* message)
{
    off_t cur_pos, off_write, str_write;
    size_t size;
    addr_t address;

    if ((cur_pos = lseek(fd, 0L, SEEK_CUR)) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Store current position

    off_write = find_available_entry(fd); // Get available entry offset
    if (off_write == -1) { // If no entry found, create new table and get next available offset
        append_table(fd, 8);
        off_write = find_available_entry(fd);
    }

    size = strlen(message) + 1; // Get size of message

    if ((str_write = lseek(fd, 0, SEEK_END)) == -1) {
        fprintf(stderr, "lseek syscall failed (EOF position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Get final position to write str to (for now)

    address.size = size;
    address.address = str_write;

    if (write(fd, message, size) == -1) {
        fprintf(stderr, "write syscall failed (writing message): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Write message at str_write

    if (lseek(fd, off_write, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (to address offset): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to position to write address to

    if (write(fd, &address, sizeof(addr_t)) == -1) {
        fprintf(stderr, "write syscall failed (writing address): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Write address at off_write

    if (lseek(fd, cur_pos, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go back to current position
}

void print_all_str(int fd)
{
    off_t cur_pos;
    table_t table;
    int x;
    char* print;

    if ((cur_pos = lseek(fd, 0L, SEEK_CUR)) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Store current position

    if (lseek(fd, 0L, SEEK_SET)) {
        fprintf(stderr, "lseek syscall failed (to beginning of file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go to beginning of file

    do {
        read_table(fd, &table); // Read current table

        if (lseek(fd, table.next_table, SEEK_SET) == -1) {
            fprintf(stderr, "lseek syscall failed (to next table): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Go to next address table

        for (x = 0; x < table.table_size; x++) {
            if (table.addresses[x].size != 0) {
                if ((print = malloc(table.addresses[x].size)) == NULL) {
                    fprintf(stderr, "malloc syscall failed (allocate str): %s\n", strerror(errno));
                    exit(EXIT_FAILURE);
                } // Allocate string

                if (lseek(fd, table.addresses[x].address, SEEK_SET) == -1) {
                    fprintf(stderr, "lseek syscall failed (to string offset): %s\n", strerror(errno));
                    exit(EXIT_FAILURE);
                } // Go to string offset

                if (read(fd, print, table.addresses[x].size) == -1) {
                    fprintf(stderr, "read syscall failed (read string): %s\n", strerror(errno));
                    exit(EXIT_FAILURE);
                } // Read string

                printf("String at %ld : %s\n", table.addresses[x].address, print);
                
                free(print);
            }
        }

        free(table.addresses);
    } while (table.next_table != 0L);

    if (lseek(fd, cur_pos, SEEK_SET) == -1) {
        fprintf(stderr, "lseek syscall failed (current position): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Go back to current position
}