#include "main_helper.h"
#include "file_functions.h"
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int open_file(char* file_name)
{
    int nadr, nvoidadr, fd;

    nadr = 0;
    nvoidadr = 0;

    if ((fd = open(file_name, O_RDWR, S_IRUSR | S_IWUSR)) == -1) {
        if (errno != ENOENT) {
            fprintf(stderr, "open syscall failed (opening file): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        errno = 0;

        puts("File does not exist.");

        while (nadr <= 0) {
            printf("Number of addresses:\n> ");
            if (scanf("%d", &nadr) != 1) {
                fprintf(stderr, "Couldn't process input.\n");
                
                empty_stdin();
            }
        }

        while (nvoidadr <= 0) {
            printf("Number of void addresses:\n> ");
            if (scanf("%d", &nvoidadr) != 1) {
                fprintf(stderr, "Couldn't process input.\n");
                
                empty_stdin();
            }
        }

        puts("Creating file...");
        if ((fd = open(file_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR)) == -1) {
            fprintf(stderr, "open syscall failed (opening file): %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        write_table(fd, -1, nadr);
        write_void_table(fd, -1, nvoidadr);

        puts("Done.");
    }

    return fd;
}

void close_file(int fd)
{
    if (close(fd) == -1) {
        fprintf(stderr, "close syscall failed (closing file): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}

char* get_str(int max_size)
{
    char* ret;
    ret = calloc(max_size, sizeof(char));

    empty_stdin();

    printf("Write a string.\n> ");
    while (fgets(ret, max_size - 1, stdin) != ret || strlen(ret) <= 1) {
        fprintf(stderr, "Failed reading input.\n> ");
        empty_stdin();
    }

    ret[strlen(ret) - 1] = '\0';

    return ret;
}

int choice(int i)
{
    int ret;

    ret = 0;

    while ((ret < 1 || ret > i) && ret != -1) {
        printf("Choice:\n> ");
        if (scanf("%d", &ret) != 1) {
            fprintf(stderr, "Couldn't process input.\n");
            empty_stdin();
        }
    }

    return ret;
}

void empty_stdin()
{
    char c;
    while (((c = getchar()) != '\n') || c == EOF);
}