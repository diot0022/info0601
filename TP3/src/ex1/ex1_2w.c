#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "personne.h"

void write_personne(int fd, personne_t* personne);

int main(int argc, char* argv[]) {
    char* filename = "personne.bin";
    int fd, age;
    char nom[256];
    char prenom[256];
    personne_t personne;

    if (argc != 1) {
        fprintf(stderr, "Usage:\t./ex1\n");
        exit(EXIT_FAILURE);
    }

    fd = open(filename, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        perror("Erreur dans l'ouverture du fichier ");
        exit(EXIT_FAILURE);
    }

    printf("Saisissez un nom (255 caractères) : ");
    if (scanf("%255s", nom) != 1) {
        fprintf(stderr, "Erreur dans la lecture du nom ");
        exit(EXIT_FAILURE);
    }

    printf("Saisissez un prénom (255 caractères) : ");
    if (scanf("%255s", prenom) != 1) {
        fprintf(stderr, "Erreur dans la lecture du prénom ");
        exit(EXIT_FAILURE);
    }

    printf("Saisissez un âge : ");
    if (scanf("%d", &age) != 1) {
        fprintf(stderr, "Erreur dans la lecture de l'âge ");
        exit(EXIT_FAILURE);
    }

    personne.nom = nom;
    personne.prenom = prenom;
    personne.age = age;

    write_personne(fd, &personne);

    if (close(fd) == -1) {
        perror("Erreur dans la fermeture du fichier ");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}

void write_personne(int fd, personne_t* personne)
{
    //On peut économiser 3 appels système ici avec un void*, mais le code devient exécrable
    int sizefn, sizeln;

    sizefn = strlen(personne->nom);
    sizeln = strlen(personne->prenom);

    if (write(fd, &sizefn, sizeof(int)) == -1) {
        perror("Erreur dans l'écriture de la taille du nom ");
        exit(EXIT_FAILURE);
    }

    if (write(fd, personne->nom, sizefn) == -1) {
        perror("Erreur dans l'écriture du nom ");
        exit(EXIT_FAILURE);
    }

    if (write(fd, &sizeln, sizeof(int)) == -1) {
        perror("Erreur dans l'écriture de la taille du prénom ");
        exit(EXIT_FAILURE);
    }

    if (write(fd, personne->prenom, sizeln) == -1) {
        perror("Erreur dans l'écriture du prénom ");
        exit(EXIT_FAILURE);
    }

    if (write(fd, &personne->age, sizeof(int)) == -1) {
        perror("Erreur dans l'écriture de l'âge ");
        exit(EXIT_FAILURE);
    }
}