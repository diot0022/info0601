#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "personne.h"

void read_personne(int fd, personne_t* personne);

int main(int argc, char* argv[]) {
    char* filename = "personne.bin";
    int fd;
    personne_t personne;

    if (argc != 1) {
        fprintf(stderr, "Usage:\t./ex1\n");
        exit(EXIT_FAILURE);
    }

    fd = open(filename, O_RDONLY, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        perror("Erreur dans l'ouverture du fichier ");
        exit(EXIT_FAILURE);
    }

    read_personne(fd, &personne);

    printf("(Personne lue)\nNom: %s\nPrénom: %s\nÂge: %d\n", personne.nom, personne.prenom, personne.age);

    if (close(fd) == -1) {
        perror("Erreur dans la fermeture du fichier ");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}

void read_personne(int fd, personne_t* personne)
{
    //On peut économiser des appels système ici aussi... mais j'ai la flemme
    int sizefn, sizeln;

    if (read(fd, &sizefn, sizeof(int)) == -1) {
        perror("Erreur dans la lecture de la taille du nom ");
        exit(EXIT_FAILURE);
    }

    personne->nom = malloc(sizefn);

    if (read(fd, personne->nom, sizefn) == -1) {
        perror("Erreur dans la lecture du nom ");
        exit(EXIT_FAILURE);
    }

    if (read(fd, &sizeln, sizeof(int)) == -1) {
        perror("Erreur dans la lecture de la taille du prénom ");
        exit(EXIT_FAILURE);
    }

    personne->prenom = malloc(sizeln);

    if (read(fd, personne->prenom, sizeln) == -1) {
        perror("Erreur dans la lecture du prénom ");
        exit(EXIT_FAILURE);
    }

    if (read(fd, &personne->age, sizeof(int)) == -1) {
        perror("Erreur dans la lecture de l'âge ");
        exit(EXIT_FAILURE);
    }
}