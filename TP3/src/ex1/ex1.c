#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>



int main(int argc, char* argv[]) {
    char* filename = "toto.bin";
    int fd, size, readf;
    char msg[256];
    int automate = 1;

    if (argc > 2) {
        fprintf(stderr, "Usage:\t./ex1 <file name>\n\t./ex1\n");
        exit(EXIT_FAILURE);
    }
    
    if (argc == 2)
        filename = argv[1];

    fd = open(filename, O_CREAT | O_RDWR | O_APPEND, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        perror("Erreur dans l'ouverture du fichier ");
        exit(EXIT_FAILURE);
    }

    while (automate != 2) {
        puts("1) Ajouter une chaîne de caractères");
        puts("2) Afficher le fichier puis quitter");

        if (scanf("%d", &automate) != 1) {
            fprintf(stderr, "Lecture d'un entier impossible\n");
            exit(EXIT_FAILURE);
        }

        if (automate != 1 && automate != 2) continue;

        if (automate == 1) {
            for (int x = 0; x < 256; x++) msg[x] = '\0';
            puts("Saisir une chaîne de caractères (max 255)");
            if (scanf("%255s", msg) != 1) {
                fprintf(stderr, "Lecture d'une chaîne de caractères impossible\n");
                exit(EXIT_FAILURE);
            }

            size = strlen(msg);

            printf("%d %s\n", size, msg);

            //Writing size
            if (write(fd, &size, sizeof(int)) == -1) {
                perror("Erreur dans l'écriture de la taille ");
                exit(EXIT_FAILURE);
            }

            //Writing message
            if (write(fd, msg, size) == -1) {
                perror("Erreur dans l'écriture du message ");
                exit(EXIT_FAILURE);
            }
        }
    }

    if (lseek(fd, SEEK_SET, 0) == -1) {
        perror("Erreur dans le repositionnement du fichier ");
        exit(EXIT_FAILURE);
    }

    while ((readf = read(fd, &size, sizeof(int))) != 0) {
        for (int x = 0; x < 256; x++) msg[x] = '\0';
        if (readf == -1) {
            perror("Erreur dans la lecture d'un entier ");
            exit(EXIT_FAILURE);
        }

        if (read(fd, msg, size) == -1) {
            perror("Erreur dans la lecture d'une chaîne de caractères ");
            exit(EXIT_FAILURE);
        }

        printf("%d %s\n", size, msg);
    }

    if (close(fd) == -1) {
        perror("Erreur dans la fermeture du fichier ");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}