#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

void handler(int signum, siginfo_t* info, void* unused);

pid_t client_pid = -1;

int main(void)
{
    struct sigaction action;
    sigset_t all;
    int countdown, x;

    // Fill set, blocking all signals
    sigfillset(&action.sa_mask);
    sigfillset(&all);

    // Remove SIGINT and SIGRTMIN + X from this set, allowing them to be received
    sigdelset(&action.sa_mask, SIGINT);
    for (x = 0; x < 4; x++) {
        sigdelset(&action.sa_mask, SIGRTMIN + x);

        if (sigaction(SIGRTMIN + x, &action, NULL) == -1) {
            fprintf(stderr, "Error positioning handler for SIGRTMIN + %d: %s\n", x, strerror(errno));
            exit(EXIT_FAILURE);
        }
    }

    // Set flags and handler of sigaction structure...
    action.sa_flags = 0;
    action.sa_handler = NULL;
    action.sa_sigaction = handler;

    // Block all signals
    sigprocmask(SIG_SETMASK, &all, NULL);

    // Set current thread's signal mask to the one created earlier and pause until reception of SIGINT or SIGRTMIN
    sigsuspend(&action.sa_mask);

    srand(time(NULL));
    countdown = 2;
    while (countdown != 0) {
        x = rand() % 4;
        
        sigsuspend(&action.sa_mask);
    }

    return EXIT_SUCCESS;
}

void handler(int signum, siginfo_t* info, void* unused)
{
    int send;

    if (client_pid == -1) {
        client_pid = info->si_pid;
    }
    
    if (signum >= SIGRTMIN || signum <= SIGRTMIN + 3) {
        send = rand() % 4;

        sigqueue(client_pid, SIGRTMIN, send);
    }
}