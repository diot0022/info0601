#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

int main(int argc, char* argv[])
{
    pid_t server_pid;
    int x;

    if (argc != 2) {
        fprintf(stderr, "Usage:\t./client <server_pid>.\n");
        exit(EXIT_FAILURE);
    } // Verifications

    if ((server_pid = atoi(argv[1])) <= 1) {
        fprintf(stderr, "server_pid must be an integer higher than 1.\n");
        exit(EXIT_FAILURE);
    } // Store server_pid

    if (kill(server_pid, 0) == -1) {
        fprintf(stderr, "Error pinging server_pid : %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Ping server process

    for (x = 0; x < 3; x++) {
        puts("Sending SIGUSR1 to server...");
        if (kill(server_pid, SIGUSR1) == -1) {
            fprintf(stderr, "Error sending SIGUSR1 to server process : %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } // Send SIGUSR1 to the process...

        sleep(1); // ...with a pause of 1 second...
    } // ... 3 times

    puts("Sending SIGINT to server...");
    if (kill(server_pid, SIGINT) == -1) {
        fprintf(stderr, "Error sending SIGINT to server process : %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } // Send SIGINT to the process

    return EXIT_SUCCESS;
}