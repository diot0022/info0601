#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

void handler(int signum);

int main(void)
{
    struct sigaction action;
    sigset_t all;
    sigset_t pending;

    // Fill sets, blocking all signals
    sigfillset(&action.sa_mask);
    sigfillset(&all);

    // Remove SIGINT from this set, allowing it to be received
    sigdelset(&action.sa_mask, SIGINT);

    // Set flags and handler of sigaction structure...
    action.sa_flags = 0;
    action.sa_handler = handler;

    // Position handler for SIGINT
    if (sigaction(SIGINT, &action, NULL) == -1) {
        fprintf(stderr, "Error positionning handler: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    sigprocmask(SIG_SETMASK, &all, NULL);

    printf("PID : %d\n", getpid()); // Print PID

    // Wait for SIGINT signal using mask created earlier...
    puts("Wait for SIGINT...");
    sigsuspend(&action.sa_mask);

    // Now we fetch the pending signals
    sigpending(&pending);

    puts(sigismember(&pending, SIGUSR1) ? "SIGUSR1 was received" : "SIGUSR1 wasn't received");

    return EXIT_SUCCESS;
}

void handler(int signum)
{
    //
}