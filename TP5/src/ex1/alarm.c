#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void handler(int signum);

int main(void)
{
    struct sigaction action;
    sigset_t all;

    // Print PID in case you want to kill the process...
    printf("PID: %d\n", getpid());
    
    // Create sets blocking all signals
    sigfillset(&all);
    sigfillset(&action.sa_mask);

    // Remove SIGALRM from this set to allow receiving it
    sigdelset(&action.sa_mask, SIGALRM);

    // Set flags and handler of sigaction structure...
    action.sa_flags = 0;
    action.sa_handler = handler;

    // Sigaction syscall to position a handler for SIGALRM
    if (sigaction(SIGALRM, &action, NULL) == -1) {
        perror("Error positioning handler ");
        exit(EXIT_FAILURE);
    }

    // sigprocmask here is used to block all signals until sigsuspend allows receiving and handling SIGALRM.
    // Since SIGALRM will be sent in 10 seconds, it's almost impossible that the signal would be received between alarm() and sigsuspend()
    // But, using sigprocmask gives us a guarantee, because SIGALRM will be blocked until sigsuspend changes the mask again.
    sigprocmask(SIG_SETMASK, &all, NULL);
    
    // Alarm for 10 seconds
    alarm(10);

    // Wait for SIGALRM signal using mask created earlier...
    sigsuspend(&action.sa_mask);

    return EXIT_SUCCESS;
}

void handler(int signum)
{
    puts("End of process");
}