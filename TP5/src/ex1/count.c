#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void handler(int signum);

int count = 0;
int stop = 0;

int main(void)
{
    struct sigaction action;
    sigset_t all;

    // Print PID in case you want to kill the process...
    printf("PID: %d\n", getpid());
    
    // Create a set blocking all signals
    sigfillset(&action.sa_mask);
    sigfillset(&all);

    // Remove SIGALRM from this set to allow receiving it
    sigdelset(&action.sa_mask, SIGINT);
    sigdelset(&action.sa_mask, SIGALRM);

    // Set flags and handler of sigaction structure...
    action.sa_flags = 0;
    action.sa_handler = handler;

    // Sigaction syscall to position a handler for SIGALRM and SIGINT
    if (sigaction(SIGALRM, &action, NULL) == -1) {
        perror("Error positioning handler ");
        exit(EXIT_FAILURE);
    }

    if (sigaction(SIGINT, &action, NULL) == -1) {
        perror("Error positioning handler ");
        exit(EXIT_FAILURE);
    }

    sigprocmask(SIG_SETMASK, &all, NULL);
    
    // Alarm for 10 seconds
    alarm(10);

    while (!stop) {
        sigsuspend(&action.sa_mask);
    }

    printf("\nCount : %d\n", count);

    return EXIT_SUCCESS;
}

void handler(int signum)
{
    if (signum == SIGINT) {
        count++;
        puts(" => +1");

        // Sigsuspend could be used here, but I think it could cause a stack overflow, since context will be duplicated with each signal being handled in its own handler 
    } else if (signum == SIGALRM) {
        stop = 1;
    }
}