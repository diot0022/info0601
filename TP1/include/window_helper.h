#ifndef __WINDOW_HELPER_H__
#define __WINDOW_HELPER_H__

typedef struct {
    int y;
    int x;
} w_position;

typedef struct {
    int width;
    int height;
} w_size;

#endif