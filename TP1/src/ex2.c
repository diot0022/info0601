#include <ncurses.h>
#include <stdlib.h>
#include "ncurses_helper.h"
#include "window_helper.h"

int main(void) {
    char ch;
    int width, height;

    ncurses_init();
    ncurses_colors();
    ncurses_init_mouse();
    init_colors();
    nodelay(stdscr, TRUE);

    getmaxyx(stdscr, height, width);
    w_position screen_center = {width / 2, height / 2};
    
    w_position pos_top_win = {0, 0};
    WINDOW* top_win = newwin(10, width, pos_top_win.x, pos_top_win.y);
    box(top_win, 0, 0);
    w_position pos_center_top_win = get_win_center(top_win);
    wrefresh(top_win);

    w_position pos_bottom_win = {screen_center.y, pos_top_win.x + 15};
    WINDOW* bottom_win = newwin(10, 12, pos_bottom_win.x, pos_bottom_win.y);
    box(bottom_win, 0, 0);
    wrefresh(bottom_win);

    w_position pos_text_top_win = {pos_top_win.y + 1, pos_top_win.x + 1};
    WINDOW* text_top_win = subwin(top_win, 1, 20, 1, 1);
    wrefresh(text_top_win);

    w_position pos_cross_bottom_win = {pos_bottom_win.y + 1, pos_bottom_win.x + 1};
    WINDOW* cross_bottom_win = subwin(bottom_win, 8, 10, pos_bottom_win.x + 1, pos_bottom_win.y + 1);
    wrefresh(cross_bottom_win);

    refresh();

    while ((ch = getch()) != 27) {
        w_position pos;
        if (mouse_in_win(cross_bottom_win, &pos)) {
            wclear(text_top_win);
            wclear(cross_bottom_win);

            wprintw(text_top_win, "%d %d", pos.x, pos.y);
            mvwaddch(cross_bottom_win, pos.x, pos.y, 'X');

            wrefresh(cross_bottom_win);
            wrefresh(text_top_win);
        }
    }

    ncurses_stop();

    return EXIT_SUCCESS;
}