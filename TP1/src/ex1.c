#include <ncurses.h>
#include <stdlib.h>
#include "ncurses_helper.h"
#include "window_helper.h"

int main(void) {
    char ch;
    int y, x;

    ncurses_init();
    ncurses_colors();
    ncurses_init_mouse();
    nodelay(stdscr, TRUE);
    init_colors();

    getmaxyx(stdscr, y, x);
    w_position screen_center = {y / 2, x / 2};
    w_position pos_red = {screen_center.y + 5, screen_center.x - 30};
    w_position pos_green = {screen_center.y, screen_center.x - 30};
    w_position pos_blue = {screen_center.y - 5, screen_center.x - 30};

    mvaddstr(screen_center.y, screen_center.x, "Bonjour !");

    attron(COLOR_PAIR(6));
    mvaddch(pos_red.y, pos_red.x, ' ');
    attroff(COLOR_PAIR(6));

    attron(COLOR_PAIR(7));
    mvaddch(pos_green.y, pos_green.x, ' ');
    attroff(COLOR_PAIR(7));

    attron(COLOR_PAIR(8));
    mvaddch(pos_blue.y, pos_blue.x, ' ');
    attroff(COLOR_PAIR(8));
    
    refresh();

    while ((ch = getch()) != 27) {
        if (mouse_at_pos(pos_red) || ch == '1') {
            attron(COLOR_PAIR(6));
            mvaddstr(screen_center.y, screen_center.x, "Bonjour !");
            attroff(COLOR_PAIR(6));
        } else if (mouse_at_pos(pos_green) || ch == '2') {
            attron(COLOR_PAIR(7));
            mvaddstr(screen_center.y, screen_center.x, "Bonjour !");
            attroff(COLOR_PAIR(7));
        } else if (mouse_at_pos(pos_blue) || ch == '3') {
            attron(COLOR_PAIR(8));
            mvaddstr(screen_center.y, screen_center.x, "Bonjour !");
            attroff(COLOR_PAIR(8));
        }
    }

    ncurses_stop();

    return EXIT_SUCCESS;
}