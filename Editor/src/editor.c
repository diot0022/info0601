#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "ncurses_helper.h"
#include "window_helper.h"
#include "map_data.h"

int main(void)
{
    ncurses_init();
    ncurses_colors();
    ncurses_init_mouse();
    init_colors();
    nodelay(stdscr, TRUE);

    w_size size_level = {62, 22};
    w_size size_tools = {15, 22};
    w_size size_infos = {77, 5};

    w_position pos_origin = {0, 0};
    w_position pos_level = {pos_origin.y, pos_origin.x};
    w_position pos_tools = {pos_level.y + size_level.width, pos_level.x};
    w_position pos_infos = {pos_level.y, pos_level.x + size_level.height};

    WINDOW* win_level = newwin(size_level.height, size_level.width, pos_level.x, pos_level.y);
    WINDOW* win_tools = newwin(size_tools.height, size_tools.width, pos_tools.x, pos_tools.y);
    WINDOW* win_infos = newwin(size_infos.height, size_infos.width, pos_infos.x, pos_infos.y);
    
    box(win_level, 0, 0);
    box(win_tools, 0, 0);
    box(win_infos, 0, 0);

    mvwaddstr(win_level, 0, 1, "Level");
    mvwaddstr(win_tools, 0, 1, "Tools");
    mvwaddstr(win_infos, 0, 1, "Informations");

    //Level Box [Level]
    w_size size_level_box = {size_level.height - 2, size_level.width - 2};
    WINDOW* win_level_box = derwin(win_level, size_level_box.width, size_level_box.height, 1, 1);

    wattron(win_level_box, COLOR_PAIR(BGBLUE));
    wborder(win_level_box, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    wattroff(win_level_box, COLOR_PAIR(BGBLUE));

    //Level Data [Level Box]
    w_size size_level_data = {size_level_box.height - 2, size_level_box.width - 2};
    WINDOW* win_level_data = derwin(win_level_box, size_level_data.height, size_level_data.width, 1, 1);

    //Tools [Tool Select]
    w_size size_tool_select = {size_tools.height - 2, size_tools.width - 2};
    WINDOW* win_tool_select = derwin(win_tools, size_tool_select.height, size_tool_select.width, 1, 1);

    wrefresh(win_level);
    wrefresh(win_tools);
    wrefresh(win_infos);

    w_position click; 
    while (getch() != 27) {
        if (mouse_in_win(win_level_data, &click)) {
            ladder_t l = DEFAULT_LADDER;
            display_data(win_level_data, &l, click);

            wrefresh(win_level_data);
        }

        if (mouse_in_win(win_tool_select, &click)) {
            wprintw(win_tool_select, "SIROGHRGIREGO");
            wrefresh(win_tool_select);
        }
    }

    ncurses_stop();

    return EXIT_SUCCESS;
}