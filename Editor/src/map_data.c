#include "map_data.h"

void display_data(WINDOW* win, void* data, w_position pos)
{
    data_t d = *((data_t*)data);

    switch (d.type) {
        case 0:
            wattron(win, COLOR_PAIR(BGBLUE));
            mvwaddch(win, pos.x, pos.y, ' ');
            wattroff(win, COLOR_PAIR(BGBLUE));
            break;
        case 1:
            wattron(win, COLOR_PAIR(RED));
            mvwaddch(win, pos.x, pos.y, 'V');
            wattroff(win, COLOR_PAIR(RED));
            break;
        case 2:
            wattron(win, COLOR_PAIR(RED));
            mvwaddch(win, pos.x, pos.y, 'V');
            wattroff(win, COLOR_PAIR(RED));
            break;
        case 3:
            wattron(win, COLOR_PAIR(RED));
            mvwaddch(win, pos.x, pos.y, 'V');
            wattroff(win, COLOR_PAIR(RED));
            break;
    }
}