#include "ncurses_helper.h"
#include <stdlib.h>

void ncurses_init()
{
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    refresh(); 
    curs_set(FALSE);
}

void ncurses_stop()
{
    endwin();
}

void ncurses_colors()
{
    if (has_colors() == FALSE) {
        ncurses_stop();
        fprintf(stderr, "The terminal doesn't support colors.\n");
        exit(EXIT_FAILURE);
    }

    start_color();
}

void ncurses_init_mouse()
{
    if (!mousemask(BUTTON1_PRESSED, NULL)) {
        ncurses_stop();
        fprintf(stderr, "Mouse isn't supported.\n");
        exit(EXIT_FAILURE);
    }
}


int mouse_getpos(int* x, int* y)
{
    MEVENT event;
    int result = getmouse(&event);

    if (result == OK) {
        *x = event.x;
        *y = event.y;
    }

    return result;
}

int mouse_at_pos(w_position pos)
{
    int x, y;
    mouse_getpos(&x, &y);

    return pos.x == x && pos.y == y;
}

int mouse_in_win(WINDOW* window, w_position* pos)
{
    int x, y, beg_x, beg_y, width, height;

    mouse_getpos(&y, &x);
    getbegyx(window, beg_x, beg_y);
    getmaxyx(window, width, height);

    pos->y = y - beg_y;
    pos->x = x - beg_x;

    return x >= beg_x && x < beg_x + width && y >= beg_y && y < beg_y + height;
}

w_position get_win_center(WINDOW* window)
{
    int beg_x, beg_y, max_x, max_y;

    getbegyx(window, beg_x, beg_y);
    getmaxyx(window, max_x, max_y);

    w_position ret = {max_y - beg_y, max_x - beg_x};

    return ret;
}

void init_colors()
{
    init_pair(RED, COLOR_RED, COLOR_BLACK);
    init_pair(GREEN, COLOR_GREEN, COLOR_BLACK);
    init_pair(BLUE, COLOR_BLUE, COLOR_BLACK);
    init_pair(CYAN, COLOR_CYAN, COLOR_BLACK);
    init_pair(MAGENTA, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(BGRED, COLOR_BLACK, COLOR_RED);
    init_pair(BGGREEN, COLOR_BLACK, COLOR_GREEN);
    init_pair(BGBLUE, COLOR_BLACK, COLOR_BLUE);
    init_pair(BGCYAN, COLOR_BLACK, COLOR_CYAN);
    init_pair(BGMAGENTA, COLOR_BLACK, COLOR_MAGENTA);
}