#ifndef __NCURSES_HELPER__
#define __NCURSES_HELPER__

#include "window_helper.h"
#include <ncurses.h>

#define RED 1
#define GREEN 2
#define BLUE 3
#define CYAN 4
#define MAGENTA 5
#define BGRED 6
#define BGGREEN 7
#define BGBLUE 8
#define BGCYAN 9
#define BGMAGENTA 10
#define YELLOW 11

void ncurses_init();
void ncurses_stop();
void ncurses_colors();
void ncurses_init_mouse();
int mouse_getpos(int *x, int *y);
int mouse_at_pos(w_position pos);
int mouse_in_win(WINDOW* window, w_position* pos);
w_position get_win_center(WINDOW* window);
void init_colors();

#endif
