#ifndef __MAP_DATA_H__
#define __MAP_DATA_H__

#include <ncurses.h>
#include "ncurses_helper.h"

typedef struct {
    int type;
    int color;
} data_t;

typedef struct {
    data_t data;
} block_t;
static block_t DEFAULT_BLOCK = {0, 0};

typedef struct {
    data_t data;
} life_t;
static life_t DEFAULT_LIFE = {1, 0};

typedef struct {
    data_t data;
} ladder_t;
static ladder_t DEFAULT_LADDER = {2, 0};

typedef struct {
    data_t data;
} trap_t;
static trap_t DEFAULT_TRAP = {3, 0};

void display_data(WINDOW* win, void* data, w_position pos);

#endif