# INFO0601 : Practical works

Welcome! This repository will contain all of my solutions for INFO0601 practical works. You will need [CMake](https://cmake.org/download/) and [ncurses](https://ftp.gnu.org/pub/gnu/ncurses/) to compile them.

### Status of practical works
Some practical works may be unfinished or even totally wrong. Feel free to address me any errors using push requests! Exercises explicitly linked with the project (such as the level editor) won't be listed here.