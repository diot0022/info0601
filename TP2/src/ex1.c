#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(void) {
    int fd, n;
    char str[10];

    fd = open("./toto.bin", O_CREAT | O_RDWR);
    if (fd == -1) {
        perror("Le fichier n'a pas pu être ouvert: ");
        exit(EXIT_FAILURE);
    }

    puts("Saisissez un entier et une chaîne de caractères (10 caractères) :");
    if (scanf("%d %10s", &n, str) != 2) {
        fprintf(stderr, "Problème avec scanf");
        exit(EXIT_FAILURE);
    }

    str[9] = '\0';

    if (write(fd, &n, sizeof(int)) == -1 || write(fd, str, strlen(str) * sizeof(char)) == -1) {
        perror("Impossible d'écrire dans le fichier: ");
        exit(EXIT_FAILURE);
    }

    if (close(fd) == -1) {
        perror("Le fichier n'a pas pu être fermé: ");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}