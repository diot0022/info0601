#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#define BUFFER_SIZE 4096

int main(int argc, char* argv[])
{
    int fds, fdd, n;
    char buffer[BUFFER_SIZE];

    if (argc != 3) {
        fprintf(stderr, "Nombre d'arguments incorrect ! (%d, devrait être 3)\n", argc);
        exit(EXIT_FAILURE);
    }

    fds = open(argv[1], O_RDONLY);
    if (fds == -1) {
        perror("Le fichier n'a pas pu être ouvert ");
        exit(EXIT_FAILURE);
    }

    fdd = open(argv[2], O_CREAT | O_EXCL | O_WRONLY);
    if (fdd == -1) {
        perror("Le fichier n'a pas pu être ouvert ");
        exit(EXIT_FAILURE);
    }

    while ((n = read(fds, buffer, BUFFER_SIZE)) != 0) {
        if (n == -1) {
            perror("Erreur dans la lecture du fichier ");
            exit(EXIT_FAILURE);
        }

        if (write(fdd, buffer, n) == -1) {
            perror("Erreur dans l'écriture du fichier ");
            exit(EXIT_FAILURE);
        }
    }

    if (close(fds) == -1) {
        perror("Le fichier n'a pas pu être fermé ");
        exit(EXIT_FAILURE);
    }

    if (close(fdd) == -1) {
        perror("Le fichier n'a pas pu être fermé ");
        exit(EXIT_FAILURE);
    }
}