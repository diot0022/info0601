#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#define ARRAY_SIZE 11

int main(int argc, char* argv[]) {
    int fd;
    off_t pos;
    int digits[ARRAY_SIZE];
    char w;
    struct stat fstat;
    char toto2[12] = "./toto2.bin";

    if (argc != 2) {
        fprintf(stderr, "Nombre d'arguments incorrect ! (%d, devrait être 2)\n", argc);
        exit(EXIT_FAILURE);
    }

    fd = open(argv[1], O_RDONLY);
    if (fd == -1) {
        perror("Le fichier n'a pas pu être ouvert  ");
        exit(EXIT_FAILURE);
    }

    pos = lseek(fd, 0, SEEK_CUR);
    if (pos == -1) {
        perror("Déplacement impossible ");
        exit(EXIT_FAILURE);
    }
    printf("Position dans le fichier %s : %ld\n", argv[1], pos);

    if (close(fd) == -1) {
        perror("Le fichier n'a pas pu être fermé  ");
        exit(EXIT_FAILURE);
    }

    //////////////////////////////////////////////

    for (int x = 0; x < ARRAY_SIZE; x++) digits[x] = x + 1;

    fd = open(toto2, O_CREAT | O_RDWR);
    if (fd == -1) {
        perror("Le fichier n'a pas pu être ouvert  ");
        exit(EXIT_FAILURE);
    }

    if (write(fd, digits, sizeof(int) * ARRAY_SIZE) == -1) {
        perror("Impossible d'écrire dans le fichier  ");
        exit(EXIT_FAILURE);
    }

    pos = lseek(fd, 0, SEEK_CUR);
    if (pos == -1) {
        perror("Déplacement impossible ");
        exit(EXIT_FAILURE);
    }
    printf("Position dans le fichier %s : %ld\n", toto2, pos);

    puts("Déplacement à la position 1024...");
    pos = lseek(fd, 1024, SEEK_SET);
    if (pos == -1) {
        perror("Déplacement impossible ");
        exit(EXIT_FAILURE);
    }

    puts("Ecriture d'un caractère à cette position...");
    w = 'c';
    if (write(fd, &w, sizeof(char)) == -1) {
        perror("Impossible d'écrire dans le fichier  ");
        exit(EXIT_FAILURE);
    }

    if (stat(toto2, &fstat) == -1) {
        perror("Lecture des stats impossible   ");
        exit(EXIT_FAILURE);
    }

    printf("Taille: %ld, nombre de blocs: %ld, etc...\n", fstat.st_size, fstat.st_blksize);

    if (close(fd) == -1) {
        perror("Le fichier n'a pas pu être fermé ");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}