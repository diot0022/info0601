#ifndef _INCLUDE_
#define _INCLUDE_

#define HEIGHT               20    // Min. height for the interface
#define WIDTH                20    // Min. width for the interface
#define MAX_MSG              256   // Max. size of a message

// Structure of a message
typedef struct {
    long type;
    char message[MAX_MSG];
} message_t;

#endif