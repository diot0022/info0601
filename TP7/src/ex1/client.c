#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <ncurses.h>

#include "include.h"

#define xstr(s) str(s)
#define str(s) #s

int main(int argc, char *argv[]) {
    int msqid;
    message_t msg;
    bool stop = FALSE;
    char c;
    
    // Get the message queue
    if ((msqid = msgget(KEY, S_IWUSR | S_IRUSR)) == -1) {
        perror("msgget() syscall failed: ");
        exit(EXIT_FAILURE);
    }
    
    // Main loop
    printf("Type messages; #QUIT# to quit, #STOP# to stop the viewer\n");
    while(stop == FALSE) {
        printf("Type a message: ");
        if(scanf("%"xstr(MAX_MSG)"[^\n]", msg.message) != 1) {
            fprintf(stderr, "Nothing to read\n");
            exit(EXIT_FAILURE);
        }
        while(((c = getchar()) != '\n') || (c == EOF));

        if(strcmp(msg.message, "#QUIT#") == 0)
            stop = TRUE;
        else {
            // Send the message
            
            if (msgsnd(msqid, &msg, sizeof(message_t) - sizeof(long), 0) == -1) {
                perror("msgsnd() syscall failed: ");
                exit(EXIT_FAILURE);
            }

            printf("Message sent.\n");
            
            if(strcmp(msg.message, "#STOP#") == 0)
                stop = TRUE;
        }
    }
    
    return EXIT_SUCCESS;
}