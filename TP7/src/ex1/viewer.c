#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <locale.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>

#include "functions.h"
#include "window.h"
#include "colors.h"
#include "include.h"

int main(int argc, char *argv[]) {
    bool stop = FALSE;
    int msqid;
    window_t *win;
    message_t msg;
    
    // ncurses initialization
    setlocale(LC_ALL, "");
    ncurses_init();
    ncurses_init_mouse();
    ncurses_colors(); 
    palette();
    clear();
    refresh();  
    
    // Check terminal size
    if((COLS < WIDTH) || (LINES < HEIGHT)) {
        ncurses_stop();
        fprintf(stderr, 
              "Dimensions are invalid (%d,%d). width must be greater than %d and height mut be greater than %d\n",
              COLS, LINES, WIDTH, HEIGHT);
        exit(EXIT_FAILURE);
    }
    
    // Create window to display messages
    win = window_create(0, 0, COLS, HEIGHT, "Messages", TRUE);
    
    // Create/get the message queue
    if ((msqid = msgget(KEY, IPC_CREAT | S_IWUSR | S_IRUSR)) == -1) {
        perror("msgget() syscall failed: ");
        exit(EXIT_FAILURE);
    }
    
    // Main lool
    while(stop == FALSE) {
        // Receive message
        if (msgrcv(msqid, &msg, sizeof(message_t), 0, 0) == -1) {
            perror("msgrcv() syscall failed: ");
            exit(EXIT_FAILURE);
        }
        
        // Stop or display message
        if(strcmp(msg.message, "#STOP#") == 0) {            
            stop = TRUE;
            window_printw(win, "Stop request received...\n");
            window_refresh(win);
            sleep(5);
        }
        else {
            window_printw(win, "%s\n", msg.message);
            window_refresh(win);
        }
    }
    
    // Stop ncurses
    window_delete(&win);
    ncurses_stop();

    // Delete queue
    if (msgctl(KEY, IPC_RMID, NULL) == -1) {
        perror("msgctl() syscall failed (deleting queue): ");
        exit(EXIT_FAILURE);
    }

    printf("Queue deleted.\n");

    return EXIT_SUCCESS;    
}