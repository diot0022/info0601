#include <sys/stat.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

int main(void)
{
    int msqid;

    if ((msqid = msgget(KEY, IPC_CREAT | S_IRUSR | S_IWUSR)) == -1) {
        fprintf(stderr, "msgget() syscall failed (creating or getting queue): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (msgctl(msqid, IPC_RMID, NULL) == -1) {
        fprintf(stderr, "msgctl() syscall failed (deleting queue): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}