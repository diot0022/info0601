#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <unistd.h>

int main(void)
{
    int msqid;
    struct sigaction action;
    sigset_t all;


    // Block all signals
    sigfillset(&all);
    sigprocmask(SIG_SETMASK, &all, NULL);

    // Fill set, blocking all signals
    sigfillset(&action.sa_mask);

    // Remove SIGINT from this set, allowing it to be received
    sigdelset(&action.sa_mask, SIGINT);

    // Set flags and handler of sigaction structure...
    action.sa_flags = 0;
    action.sa_handler = handler;

    // Position handler for SIGINT
    if (sigaction(SIGINT, &action, NULL) == -1) {
        fprintf(stderr, "Error positionning handler: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }


    // Get queue
    if ((msqid = msgget(KEY, S_IRUSR | S_IWUSR)) == -1) {
        fprintf(stderr, "msgget() syscall failed (getting queue): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}