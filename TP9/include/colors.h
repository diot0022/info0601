#ifndef _COLORS_
#define _COLORS_

// Colors constants
#define WHITE       1
#define GREEN       2
#define BLUE        3
#define RED         4 
#define YELLOW      5
#define CYAN        6
#define MAGENTA     7
#define BK_WHITE    8
#define BK_GREEN    9
#define BK_BLUE    10
#define BK_RED     11
#define BK_YELLOW  12
#define BK_CYAN    13
#define BK_MAGENTA 14

/**
 * Palette definition.
 */
void palette();

#endif