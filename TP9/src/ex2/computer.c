#include <stdio.h>
#include <stdlib.h>

/**
 * Main function.
 * @param argc the number of arguments
 * @param argv arguments
 * @return the return code
 */
int main(int argc, char *argv[]) {
    printf("Nothing here...\n");

    return EXIT_SUCCESS;    
}