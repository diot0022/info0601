#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "share.h"

int main(int argc, char* argv[])
{
    memshared_t mem;
    request_t request;
    int x;

    if (argc <= 2) {
        fprintf(stderr, "Usage:\t./shared_server <fifo_name> [values].\n");
        exit(EXIT_FAILURE);
    }

    init_memshared_atoi(&mem, argv[1], argc - 2, argv + 2);

    printf("Fifo at %s.\nShared array initialized with: ", mem.path);
    for (x = 0; x < mem.size; x++) {
        printf(x == mem.size - 1 ? "%d\n" : "%d, ", mem.array[x]);
    }

    puts("Waiting for client...");
    wait_for_client(&mem);

    puts("Destroying fifo and shared array...");
    destroy_memshared(&mem);

    return EXIT_SUCCESS;
}