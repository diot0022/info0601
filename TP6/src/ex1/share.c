#include "share.h"
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>

void init_memshared(memshared_t* m, char* path, size_t size, ...)
{
    va_list arglist;
    int x;
    int* array;

    if ((array = malloc(sizeof(int) * size)) == NULL) {
        fprintf(stderr, "Malloc of size %ld failed: %s\n", size, strerror(errno));
        exit(EXIT_FAILURE);
    }

    va_start(arglist, size);
    for (x = 0; x < size; x++) {
        array[x] = va_arg(arglist, int);
    }
    va_end(arglist);

    copy_memshared(m, path, size, array);

    free(array);
}

void init_memshared_atoi(memshared_t* m, char* path, size_t size, char** array)
{
    int x;
    int* int_array;

    if ((int_array = malloc(sizeof(int) * size)) == NULL) {
        fprintf(stderr, "Malloc of size %ld failed: %s\n", size, strerror(errno));
        exit(EXIT_FAILURE);
    }

    for (x = 0; x < size; x++) {
        int_array[x] = atoi(array[x]);
    }

    copy_memshared(m, path, size, int_array);

    free(int_array);
}

void copy_memshared(memshared_t* m, char* path, size_t size, int* array)
{
    int x;
    m->size = size;
    m->array = malloc(sizeof(int) * m->size);

    if (m->array == NULL) {
        fprintf(stderr, "Malloc of size %ld failed: %s\n", size, strerror(errno));
        exit(EXIT_FAILURE);
    }

    strncpy(m->path, "/tmp/", 5);
    strcpy(m->path + 5, path);

    if ((m->fd = mkfifo(m->path, S_IRUSR)) == -1) {
        fprintf(stderr, "Creating fifo at %s failed: %s\n", m->path, strerror(errno));
        free(m->array);
        exit(EXIT_FAILURE);
    }
    
    for (x = 0; x < m->size; x++) m->array[x] = array[x];
}

void destroy_memshared(memshared_t* m)
{
    free(m->array);
    m->size = 0L;
    m->array = NULL;

    if (close(m->fd) == -1) {
        fprintf(stderr, "Closing fifo %s failed: %s\n", m->path, strerror(errno));
        exit(EXIT_SUCCESS);
    }

    if (unlink(m->path) == -1) {
        fprintf(stderr, "Unlink fifo %s failed: %s\n", m->path, strerror(errno));
        exit(EXIT_SUCCESS);
    }

    m->fd = -1;
    memcpy(m->path, "\0", 256);
}

void wait_for_client(memshared_t* m)
{
    if ((m->fd = open(m->path, O_RDONLY, S_IRUSR)) == -1) {
        fprintf(stderr, "Openning fifo %s failed: %s\n", m->path, strerror(errno));
        exit(EXIT_SUCCESS);
    }
}

int get_size(memshared_t* m)
{
    return m->size;
}

int get_at_index(memshared_t* m, long index)
{
    return (index >= 0 && index < m->size) ? m->array[index] : -1;
}

void set_at_index(memshared_t* m, long index, int value)
{
    if (index >= 0 && index < m->size) {
        m->array[index] = value;
    }
}

void init_request(request_t* r, pid_t source_pid, int type, ...)
{
    va_list arglist;

    r->source_pid = source_pid;
    r->type = type;

    va_start(arglist, type);

    switch (type) {
        case GET:
            r->key = va_arg(arglist, int);
            break;
        case SET:
            r->key = va_arg(arglist, int);
            r->value = va_arg(arglist, int);
            break;
        case SIZE:
            // Size request has no parameters
            break;
    }

    va_end(arglist);
}


void destroy_request(request_t* r)
{
    r->source_pid = -1;
    r->type = -1;
    r->key = -1;
    r->value = -1;
}