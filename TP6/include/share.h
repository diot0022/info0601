#ifndef __SHARE_H__
#define __SHARE_H__

#include <stdarg.h>
#include <unistd.h>

#define GET 0
#define SET 1
#define SIZE 2
#define STOP 3

typedef struct {
    size_t size;
    int* array;
    int fd;
    char path[256];
} memshared_t;

void init_memshared(memshared_t* m, char* path, size_t size, ...);
void init_memshared_atoi(memshared_t* m, char* path, size_t size, char** array);
void copy_memshared(memshared_t* m, char* path, size_t size, int* array);
void destroy_memshared(memshared_t* m);

void wait_for_client(memshared_t* m);

int get_size(memshared_t* m);
int get_at_index(memshared_t* m, long index);
void set_at_index(memshared_t* m, long index, int value);

typedef struct {
    pid_t source_pid;
    int type;
    long key;
    int value;
} request_t;

void init_request(request_t* r, pid_t source_pid, int type, ...);
void destroy_request(request_t* r);

#endif