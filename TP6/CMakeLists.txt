cmake_minimum_required(VERSION 3.0)

project (TP6 C)

#Flags
add_compile_options(-Wall -Werror)
add_link_options(-Wall -Werror)

#Dossier sources
set(SRC src)

#Dossier inclusion
set(INC include)

#Sources C shared_server
set(src_shared_server "shared_server.c;share.c")
list(TRANSFORM src_shared_server PREPEND ${SRC}/ex1/)

include_directories(${INC})

add_executable(shared_server ${src_shared_server})