#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define PIPE_SIZE 8192 // On my system, data races start when PIPE_SIZE is > 8192 (because the logical pipe size is 8192)
#define ITERATIONS 100
#define FORKS 50

int child(int fd, int number);

int main(void)
{
    int p[2];
    int x, y;
    int check;
    char buffer[PIPE_SIZE];

    puts("This program will demonstrate data race conditions with pipes when writing data bigger than the pipe's size.");
    puts("Change the PIPE_SIZE, ITERATIONS and FORKS number in the code before recompiling to test it out.");
    printf("Checking with size %d and %d iterations...\n", PIPE_SIZE, ITERATIONS);

    if (pipe(p) == -1) {
        perror("Failed creating pipe ");
        exit(EXIT_FAILURE);
    }

    for (x = 0; x < FORKS; x++) {
        if ((check = fork()) == 0) child(p[1], x);
        if (check == -1) {
            perror("Failed creating fork ");
            exit(EXIT_FAILURE);
        }
    }

    check = 0;
    for (x = 0; x < ITERATIONS; x++) {
        if (read(p[0], buffer, PIPE_SIZE) == -1) {
            perror("Failed reading from pipe ");
            exit(EXIT_FAILURE);
        }

        y = 1;
        while (y < PIPE_SIZE && buffer[y - 1] == buffer[y]) y++;

        if (y != PIPE_SIZE) {
            check++;
        }
    }

    printf("%d data races detected\n", check);

    return EXIT_SUCCESS;
}

int child(int fd, int number)
{
    int x;
    char buffer[PIPE_SIZE];

    for (x = 0; x < PIPE_SIZE; x++) buffer[x] = 'a' + number;

    for (x = 0; x < ITERATIONS; x++) {
        if (write(fd, buffer, PIPE_SIZE) == -1) {
            perror("Failed writing to pipe ");
            exit(EXIT_FAILURE);
        }
    }

    exit(EXIT_SUCCESS);
}