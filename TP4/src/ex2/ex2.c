#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define N 20

int main(void)
{
    pid_t forks[N];
    int pipesFtoC[N][2];
    int pipesCtoF[N][2];
    int x, y;
    int choice;
    char msg[256];
    char* stop = "STOP";
    char* valid = "Received!";
    size_t size;
    char c;

    for (int x = 0; x < N; x++) { // Creating all pipes
        if (pipe(pipesFtoC[x]) == -1) {
            perror("[PARENT] Creating pipe father to child failed ");
            exit(EXIT_FAILURE);
        }

        if (pipe(pipesCtoF[x]) == -1) {
            perror("[PARENT] Creating pipe child to father failed ");
            exit(EXIT_FAILURE);
        }
    }

    for (x = 0; x < N; x++) { // <Child>
        if ((forks[x] = fork()) == 0) { // Fork creationg
            forks[x] = getpid();
            for (y = 0; y < N; y++) {
                if (close(pipesFtoC[y][1]) == -1) { // Closing all writing file descriptors
                    perror("[CHILD] Closing writing FtoC pipe failed ");
                    exit(EXIT_FAILURE);
                }

                if (close(pipesCtoF[y][0]) == -1) { // Closing all reading file descriptors
                    perror("[CHILD] Closing writing CtoF pipe failed ");
                    exit(EXIT_FAILURE);
                }

                if (x != y) {
                    if (close(pipesFtoC[y][0]) == -1) { // Closing all other reading file descriptors
                        perror("[CHILD] Closing reading FtoC pipe failed ");
                        exit(EXIT_FAILURE);
                    }

                    if (close(pipesCtoF[y][1]) == -1) { // Closing all other reading file descriptors
                        perror("[CHILD] Closing reading CtoF pipe failed ");
                        exit(EXIT_FAILURE);
                    }
                }
            }

            while (strcmp(msg, "STOP") != 0) {
                if (read(pipesFtoC[x][0], &size, sizeof(size_t)) == -1) {
                    perror("[CHILD] Error reading size from pipe ");
                    exit(EXIT_FAILURE);
                }

                if (read(pipesFtoC[x][0], msg, size) == -1) {
                    perror("[CHILD] Error reading message from pipe ");
                    exit(EXIT_FAILURE);
                }

                printf("[CHILD %d] Read from pipe : %s\n", forks[x], msg);

                size = strlen(valid) + 1;
                if (write(pipesCtoF[x][1], &size, sizeof(size_t)) == -1) {
                    perror("[CHILD] Error writing size to pipe ");
                    exit(EXIT_FAILURE);
                }

                if (write(pipesCtoF[x][1], valid, size) == -1) {
                    perror("[CHILD] Error writing validation message to pipe ");
                    exit(EXIT_FAILURE);
                }
            }

            printf("[CHILD %d] Bye bye!\n", forks[x]);

            exit(EXIT_SUCCESS);
        } // </Child>

        if (forks[x] == -1) {
            perror("[PARENT] Creating fork failed ");
            exit(EXIT_FAILURE);
        }
    }

    for (x = 0; x < N; x++) {
        if (close(pipesFtoC[x][0]) == -1) {
            perror("[PARENT] Closing reading pipe failed "); // Closing all reading file descriptors in parent
            exit(EXIT_FAILURE);
        }

        if (close(pipesCtoF[x][1]) == -1) {
            perror("[PARENT] Closing writing pipe failed "); // Closing all reading file descriptors in parent
            exit(EXIT_FAILURE);
        }
    }

    for (x = 0; x < N; x++) {
        printf("(%d) PID : %d\n", (x + 1), forks[x]);
    }

    while (choice != -1) {
        choice = 0;
        while (choice < -1 || choice > N || choice == 0) {
            puts("Enter a choice and a message to send (max 255 characters).");
            printf("> ");

            if (scanf("%d %255s", &choice, msg) != 2) {
                fprintf(stderr, "scanf failed.");
                exit(EXIT_FAILURE);
            }

            while (((c = getchar()) != '\n') || c == EOF);
        }

        if (choice == -1) break;

        size = strlen(msg) + 1;

        if (write(pipesFtoC[choice - 1][1], &size, sizeof(size_t)) == -1) { // Write size
            perror("[PARENT] Error writing size to pipe ");
            exit(EXIT_FAILURE);
        }

        if (write(pipesFtoC[choice - 1][1], msg, strlen(msg) + 1) == -1) { // Write message
            perror("[PARENT] Error writing message to pipe ");
            exit(EXIT_FAILURE);
        }

        if (read(pipesCtoF[choice - 1][0], &size, sizeof(size_t)) == -1) {
            perror("[PARENT] Error reading size from pipe ");
            exit(EXIT_FAILURE);
        }

        if (read(pipesCtoF[choice - 1][0], msg, size) == -1) {
            perror("[PARENT] Error reading validation message from pipe ");
            exit(EXIT_FAILURE);
        }

        printf("%s\n", msg);
    }

    size = strlen(stop) + 1;
    for (x = 0; x < N; x++) {
        if (write(pipesFtoC[x][1], &size, sizeof(size_t)) == -1) { // Write size
            perror("[PARENT] Error writing size to pipe ");
            exit(EXIT_FAILURE);
        }

        if (write(pipesFtoC[x][1], stop, strlen(stop) + 1) == -1) { // Write message
            perror("[PARENT] Error writing message to pipe ");
            exit(EXIT_FAILURE);
        }
    }

    for (x = 0; x < N; x++) {
        if (wait(NULL) == -1) {
            perror("Error calling wait ");
            exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}