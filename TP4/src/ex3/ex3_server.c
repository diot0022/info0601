#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, char* argv[])
{
    char c;
    int n, x, choice;
    char fileName[20];
    char msg[256];
    int* fds_write;
    int* fds_read;
    size_t size;

    if (argc != 2) {
        fprintf(stderr, "Usage:\t./ex3_server <n_pipes>.\n");
        exit(EXIT_FAILURE);
    }

    if ((n = atoi(argv[1])) <= 0) {
        fprintf(stderr, "<n_pipes> has to be a number higher than 0.");
        exit(EXIT_FAILURE);
    }

    //Allocate file descriptors
    if ((fds_write = malloc(sizeof(int) * n)) == NULL) {
        perror("Allocation error ");
        exit(EXIT_FAILURE);
    }

    if ((fds_read = malloc(sizeof(int) * n)) == NULL) {
        perror("Allocation error ");
        exit(EXIT_FAILURE);
    }

    // Create writing and reading file descriptors and named pipes
    for (x = 0; x < n; x++) {
        sprintf(fileName, "./toto%d%c", x + 1, '\0');
        if (mkfifo(fileName, S_IRUSR | S_IWUSR) == -1) {
            perror("Error creating writing named pipe ");
            exit(EXIT_FAILURE);
        }

        sprintf(fileName, "./toto%d_r%c", x + 1, '\0');
        if (mkfifo(fileName, S_IRUSR | S_IWUSR) == -1) {
            perror("Error creating reading named pipe ");
            exit(EXIT_FAILURE);
        }
    }

    puts("Waiting for all clients to open writing pipes...");
    for (x = 0; x < n; x++) {
        sprintf(fileName, "./toto%d%c", x + 1, '\0');
        if ((fds_write[x] = open(fileName, O_WRONLY, S_IWUSR)) == -1) {
            perror("Error opening named pipe ");
            exit(EXIT_FAILURE);
        }
    }

    puts("Opening all reading pipes...");
    for (x = 0; x < n; x++) {
        sprintf(fileName, "./toto%d_r%c", x + 1, '\0');
        if ((fds_read[x] = open(fileName, O_RDONLY, S_IRUSR)) == -1) {
            perror("Error opening named pipe ");
            exit(EXIT_FAILURE);
        }
    }

    // Main loop to send msg to different pipes
    while (choice != -1) {
        choice = 0;
        while (choice < -1 || choice > n || choice == 0) {
            printf("Select a named pipe (between 1 and %d) to send a message to.\n> ", n);
            while (scanf("%d", &choice) != 1) {
                fprintf(stderr, "Error scanning for choice.\n> ");
                while(((c = getchar()) != '\n') || c == EOF);
            }

            getchar();
        }

        if (choice != -1) {
            printf("Type a message to send to the pipe. (max 255 characters)\n> ");

            while (fgets(msg, 256, stdin) == NULL || strlen(msg) == 1) {
                fprintf(stderr, "Error scanning message.\n> ");
            }

            msg[strlen(msg) - 1] = '\0';

            sprintf(fileName, "./toto%d%c", choice, '\0');
            printf("Choice : %s\n", fileName);

            size = strlen(msg) + 1;
            if (write(fds_write[choice - 1], &size, sizeof(size_t)) == -1) {
                perror("Failed writing size to pipe ");
                exit(EXIT_FAILURE);
            }

            if (write(fds_write[choice - 1], msg, size) == -1) {
                perror("Failed writing message to pipe ");
                exit(EXIT_FAILURE);
            }
            printf("Sent message to (%s).\n", fileName);

            if (read(fds_read[choice - 1], &size, sizeof(size_t)) == -1) {
                perror("Failed reading size from pipe ");
                exit(EXIT_FAILURE);
            }

            if (read(fds_read[choice - 1], msg, size) == -1) {
                perror("Failed reading message from pipe ");
                exit(EXIT_FAILURE);
            }
            printf("Response from (%s): %s\n", fileName, msg);
        }
    }

    // Sending stop signal to all clients
    for (x = 0; x < n; x++) {
        strcpy(msg, "STOP");
        size = strlen(msg) + 1;
        sprintf(fileName, "./toto%d%c", x + 1, '\0');

        // Sending stop signal
        if (write(fds_write[x], &size, sizeof(size_t)) == -1) {
            perror("Failed writing size to pipe ");
            exit(EXIT_FAILURE);
        }

        if (write(fds_write[x], msg, size) == -1) {
            perror("Failed writing message to pipe ");
            exit(EXIT_FAILURE);
        }

        // Wait for confirmation
        if (read(fds_read[x], &size, sizeof(size_t)) == -1) {
            perror("Failed reading size from pipe ");
            exit(EXIT_FAILURE);
        }

        if (read(fds_read[x], msg, size) == -1) {
            perror("Failed reading message from pipe ");
            exit(EXIT_FAILURE);
        }
        printf("Received response from (%s): %s\n", fileName, msg);
    }

    // Close file descriptors and unlink named pipes
    for (x = 0; x < n; x++) {
        sprintf(fileName, "./toto%d%c", x + 1, '\0');

        if (close(fds_write[x]) == -1) {
            perror("Error closing file descriptor ");
            exit(EXIT_FAILURE);
        }

        if (unlink(fileName) == -1) {
            perror("Error unlinking named pipe ");
            exit(EXIT_FAILURE);
        }

        sprintf(fileName, "./toto%d_r%c", x + 1, '\0');
        if (close(fds_read[x]) == -1) {
            perror("Error closing file descriptor ");
            exit(EXIT_FAILURE);
        }

        if (unlink(fileName) == -1) {
            perror("Error unlinking named pipe ");
            exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}