#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

int main(int argc, char* argv[])
{
    int fd_write, fd_read;
    char fileName[20];
    char message[256];
    char validation[256];
    size_t size;

    if (argc != 2) {
        fprintf(stderr, "(%s) Usage:\t./ex3_client <path_to_pipe>\n", argv[1]);
        exit(EXIT_SUCCESS);
    }

    // Opening request file descriptors
    printf("(%s) Opening request pipe...\n", argv[1]);
    if ((fd_write = open(argv[1], O_RDONLY, S_IRUSR)) == -1) {
        fprintf(stderr, "(%s) Failed opening named pipe: %s", argv[1], strerror(errno));
        exit(EXIT_FAILURE);
    }

    sprintf(fileName, "%s_r", argv[1]);
    printf("(%s) Opening response pipe...\n", argv[1]);
    if ((fd_read = open(fileName, O_WRONLY, S_IWUSR)) == -1) {
        fprintf(stderr, "(%s) Failed opening response named pipe: %s", argv[1], strerror(errno));
        exit(EXIT_FAILURE);
    }

    printf("(%s) All done! Waiting for messages now.\n", argv[1]);
    sprintf(validation, "Received !");
    while (strcmp(message, "STOP") != 0) {
        if (read(fd_write, &size, sizeof(size_t)) == -1) {
            fprintf(stderr, "(%s) Failed reading size from pipe: %s", argv[1], strerror(errno));
            exit(EXIT_FAILURE);
        }

        if (read(fd_write, message, size) == -1) {
            fprintf(stderr, "(%s) Failed reading message from pipe: %s", argv[1], strerror(errno));
            exit(EXIT_FAILURE);
        }
        printf("(%s) Read from pipe: %s\n", argv[1], message);

        size = strlen(validation) + 1;
        if (write(fd_read, &size, sizeof(size_t)) == -1) {
            fprintf(stderr, "(%s) ", argv[1]);
            perror("Failed writing validation size from pipe ");
            exit(EXIT_FAILURE);
        }

        if (write(fd_read, validation, size) == -1) {
            fprintf(stderr, "(%s) ", argv[1]);
            perror("Failed writing validation message to pipe ");
            exit(EXIT_FAILURE);
        }
        printf("(%s) Sent validation message to server.\n", argv[1]);
        fflush(stdin);
    }

    // Closing file descriptors
    if (close(fd_write) == -1) {
        fprintf(stderr, "(%s) Failed closing named pipe: %s", argv[1], strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (close(fd_read) == -1) {
        fprintf(stderr, "(%s) Failed closing named pipe: %s", argv[1], strerror(errno));
        exit(EXIT_FAILURE);
    }

    printf("(%s) Bye bye!\n", argv[1]);

    return EXIT_SUCCESS;
}