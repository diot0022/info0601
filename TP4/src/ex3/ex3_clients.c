#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
    int n, x;
    char pipeName[20];
    char* args[3] = {"./ex3_client", pipeName, NULL};

    if (argc != 2) {
        fprintf(stderr, "Usage:\t./ex3_clients <number_of_clients>.\n");
        exit(EXIT_FAILURE);
    }

    if ((n = atoi(argv[1])) <= 0) {
        fprintf(stderr, "<number_of_clients> has to be a number higher than 0.");
        exit(EXIT_FAILURE);
    }

    for (x = 0; x < n; x++) {
        if (fork() == 0) {
            sprintf(pipeName, "toto%d%c", x + 1, '\0');
            if (execv("./ex3_client", args) == -1) {
                perror("execv failed ");
                exit(EXIT_FAILURE);
            }

            exit(EXIT_SUCCESS); // In case execve fails
        }
    }

    for (x = 0; x < n; x++) {
        if (wait(NULL) == -1) {
            perror("Wait syscall failed ");
            exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}