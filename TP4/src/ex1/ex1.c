#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void child(int n);
void print_bin(int n);

int main(int argc, char* argv[])
{
    int n, x, check;
    pid_t pid;

    if (argc != 2) {
        fprintf(stderr, "Usage:\t./ex1 <n_forks>\n");
        exit(EXIT_FAILURE);
    }

    if ((n = atoi(argv[1])) <= 0) {
        fprintf(stderr, "n_forks has to be a number higher than 0.\n");
        exit(EXIT_FAILURE);
    }

    for (x = 0; x < n; x++) {
        if ((check = fork()) == 0) {
            child(x + 1);
        } else if (check == -1) {
            perror("Error creating fork ");
            exit(EXIT_FAILURE);
        }
    }

    for (x = 0; x < n; x++) {
        if ((pid = wait(&check)) == -1) {
            perror("Error waiting for fork ");
            exit(EXIT_FAILURE);
        }

        printf("N: %d; PID: %d\n", WEXITSTATUS(check), pid); // We can use WEXITSTATUS to check the real exit status, or we could also bitshift check >> sizeof(int) * 2 (check wait man page) 
    }

    return EXIT_SUCCESS;
}

void child(int n)
{
    int generated, waittime;

    srand(getpid() * n);

    generated = rand() % 256;
    printf("(%d) Generated number: %d\n", n, generated);

    waittime = rand() % 5;
    printf("(%d) Waiting %ds...\n", n, waittime);

    sleep(waittime);

    exit(n);
}