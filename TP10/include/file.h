#ifndef __FILE_H__
#define __FILE_H__

#define QUEUE_SIZE 10
#define MSG_SIZE 256

typedef struct {
    char array[QUEUE_SIZE][MSG_SIZE];
    int tail;
    int head;
    int overflow;
} queue_t;

void init_queue(queue_t* queue);
void empty_queue(queue_t* queue);
void queue(queue_t* queue, char* message);
char* unqueue(queue_t* queue);
int is_queue_full(queue_t* queue);
int is_queue_empty(queue_t* queue);

#endif