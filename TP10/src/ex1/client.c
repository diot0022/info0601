#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
    int x;
    char* ip;
    int port;
    char* message;
    int gflag;

    if (argc != 4 && argc != 5) {
        fprintf(stderr, "Usage:\t./client <ip_dest> <port_dest> [-s <message> | -g]\n");
        exit(EXIT_FAILURE);
    }

    ip = argv[1];
    port = atoi(argv[2]);

    if (port == 0) {
        fprintf(stderr, "Invalid port : %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }

    if (strcmp(argv[3], "-s") == 0) {
        if (argc != 5) {
            fprintf(stderr, "Usage:\t./client <ip_dest> <port_dest> [-s <message> | -g]\n");
            exit(EXIT_FAILURE);
        }

        message = argv[4];
        gflag = 0;
    } else if (strcmp(argv[3], "-g") == 0) {
        if (argc != 4) {
            fprintf(stderr, "Usage:\t./client <ip_dest> <port_dest> [-s <message> | -g]\n");
            exit(EXIT_FAILURE);
        }

        gflag = 1;
    } else {
        fprintf(stderr, "Usage:\t./client <ip_dest> <port_dest> [-s <message> | -g]\n");
        exit(EXIT_FAILURE);
    }

    

    return EXIT_SUCCESS;
}