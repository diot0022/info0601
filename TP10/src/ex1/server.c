#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    int port;

    if (argc != 2) {
        fprintf(stderr, "Usage:\t./server <port>\n");
        exit(EXIT_FAILURE);
    }

    if ((port = atoi(argv[1])) == 0) {
        fprintf(stderr, "Invalid port : %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}