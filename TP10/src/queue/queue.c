#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

void init_queue(queue_t* queue)
{
    queue->tail = 0;
    queue->head = 0;
    queue->overflow = 0;
}

void empty_queue(queue_t* queue)
{
    for (int x = 0; x < QUEUE_SIZE; x++)
        memset(queue->array + x, '\0', MSG_SIZE);
}

void queue(queue_t* queue, char* message)
{
    if (is_queue_full(queue)) return;

    int i = (queue->tail + 1) % QUEUE_SIZE;

    strncpy(queue->array[queue->tail], message, 255);

    queue->tail = i;
}

char* unqueue(queue_t* queue)
{
    if (is_queue_empty(queue)) return;

    queue->head = (queue->tail - 1) % QUEUE_SIZE;

    return queue->array[queue->tail];
}

int is_queue_full(queue_t* queue)
{

}

int is_queue_empty(queue_t* queue)
{
    return queue->tail == queue->head;
}